package sentryboiler

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/volatiletech/sqlboiler/v4/boil"

	"gitlab.com/bonch.dev/go-lib/stracer"
)

type Executor interface {
	boil.ContextExecutor
}

type ContextExecutorSentryWrapper struct {
	ex Executor
}

func NewWrapper(ex Executor) *ContextExecutorSentryWrapper {
	return &ContextExecutorSentryWrapper{ex: ex}
}

func (c *ContextExecutorSentryWrapper) Exec(query string, args ...interface{}) (sql.Result, error) {
	return c.ex.Exec(query, args...)
}

func (c *ContextExecutorSentryWrapper) Query(query string, args ...interface{}) (*sql.Rows, error) {
	return c.ex.Query(query, args...)
}

func (c *ContextExecutorSentryWrapper) QueryRow(query string, args ...interface{}) *sql.Row {
	return c.ex.QueryRow(query, args...)
}

func (c *ContextExecutorSentryWrapper) ExecContext(ctx context.Context, query string, args ...interface{}) (sql.Result, error) {
	ctx, span := stracer.WithContext(ctx, stracer.WithCustomType("db.sql.query", formatQuery(query, args...)))
	defer span.Finish()

	return c.ex.ExecContext(ctx, query, args...)
}

func (c *ContextExecutorSentryWrapper) QueryContext(ctx context.Context, query string, args ...interface{}) (*sql.Rows, error) {
	ctx, span := stracer.WithContext(ctx, stracer.WithCustomType("db.sql.query", formatQuery(query, args...)))
	defer span.Finish()

	return c.ex.QueryContext(ctx, query, args...)
}

func (c *ContextExecutorSentryWrapper) QueryRowContext(ctx context.Context, query string, args ...interface{}) *sql.Row {
	ctx, span := stracer.WithContext(ctx, stracer.WithCustomType("db.sql.query", formatQuery(query, args...)))
	defer span.Finish()

	return c.ex.QueryRowContext(ctx, query, args...)
}

type Beginner interface {
	Executor
	boil.ContextBeginner
}

type ContextBeginnerSentryWrapper struct {
	Executor
	ex boil.ContextBeginner
}

func NewBeginnerWrapper(ex Beginner) *ContextBeginnerSentryWrapper {
	return &ContextBeginnerSentryWrapper{
		NewWrapper(ex),
		ex,
	}
}

func (c *ContextBeginnerSentryWrapper) BeginTx(ctx context.Context, opts *sql.TxOptions) (*sql.Tx, error) {
	ctx, span := stracer.WithContext(ctx, stracer.WithType("db.sql.transaction"))
	defer span.Finish()

	return c.ex.BeginTx(ctx, opts)
}

type Transactor interface {
	Executor
	boil.ContextTransactor
}

type ContextTransactorSentryWrapper struct {
	Executor
	ex boil.ContextTransactor
}

func NewTxWrapper(ex Transactor) *ContextTransactorSentryWrapper {
	return &ContextTransactorSentryWrapper{
		NewWrapper(ex),
		ex,
	}
}

func (c *ContextTransactorSentryWrapper) Commit() error {
	return c.ex.Commit()
}

func (c *ContextTransactorSentryWrapper) Rollback() error {
	return c.ex.Rollback()
}

func formatQuery(query string, args ...interface{}) string {
	if len(args) == 0 {
		return fmt.Sprintf("Query: %s", query)
	}

	return fmt.Sprintf("Query: %s\n Args: %v", query, args)
}
