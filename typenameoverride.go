package stracer

import "sync"

var overrideTypes = map[string]string{
	"Controller": "http.controller",
	"Middleware": "middleware.handle",
	"Resource":   "resource",
	"Service":    "app.service",
	"Observer":   "queue.task",
	"Repository": "app.repository",
}

var overrideTypesMutex = sync.RWMutex{}

func typeOverride(t string) string {
	overrideTypesMutex.RLock()
	defer overrideTypesMutex.RUnlock()

	if override, ok := overrideTypes[t]; ok {
		return override
	}

	return t
}

func AddTypeOverride(from, to string) {
	overrideTypesMutex.Lock()
	defer overrideTypesMutex.Unlock()

	overrideTypes[from] = to
}
