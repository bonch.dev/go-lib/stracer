package stracer

import (
	"context"

	"github.com/getsentry/sentry-go"
)

func ToContext(span *sentry.Span) context.Context {
	return span.Context()
}

func FromContext(ctx context.Context, t Type) *sentry.Span {
	var hub = sentry.GetHubFromContext(ctx)

	if hub == nil {
		hub = sentry.CurrentHub()
	}

	hub = hub.Clone()

	return sentry.StartSpan(
		sentry.SetHubOnContext(ctx, hub),
		t.Type(),
		sentry.WithDescription(t.Name()),
	)
}

// WithContext returns wrapped context.Context with injected in it span information and span itself.
func WithContext(ctx context.Context, t Type) (context.Context, *sentry.Span) {
	span := FromContext(ctx, t)

	return ToContext(span), span
}

// Wrap runs exec wrapped in sentry.Span
func Wrap(ctx context.Context, t Type, exec func(context context.Context)) {
	span := FromContext(ctx, t)

	exec(span.Context())

	span.Finish()
}

// WrapTransaction runs exec wrapped in Transaction sentry.Span
func WrapTransaction(ctx context.Context, t Type, exec func(ctx context.Context)) {
	ctx = sentry.SetHubOnContext(
		ctx,
		func() *sentry.Hub {
			hub := sentry.GetHubFromContext(ctx)
			if hub == nil {
				hub = sentry.CurrentHub()
			}

			return hub.Clone()
		}(),
	)

	span := sentry.StartTransaction(
		ctx,
		t.Name(),
		sentry.WithOpName(t.Type()),
	)

	exec(span.Context())

	span.Status = sentry.SpanStatusOK
	span.Finish()
}

func WrapErrTransaction(ctx context.Context, t Type, exec func(ctx context.Context) error) {
	ctx = sentry.SetHubOnContext(
		ctx,
		func() *sentry.Hub {
			hub := sentry.GetHubFromContext(ctx)
			if hub == nil {
				hub = sentry.CurrentHub()
			}

			return hub.Clone()
		}(),
	)

	span := sentry.StartTransaction(
		ctx,
		t.Name(),
		sentry.WithOpName(t.Type()),
	)

	if err := exec(span.Context()); err != nil {
		span.Status = sentry.SpanStatusFailedPrecondition
		span.SetTag("error", err.Error())
	} else {
		span.Status = sentry.SpanStatusOK
	}

	span.Finish()
}

// Log adds to sentry.Span Tags json marshaled logged value
func Log(span *sentry.Span, name string, logged interface{}) {
	if span == nil || name == "" {
		return
	}

	if span.Data == nil {
		span.Data = make(map[string]interface{})
	}

	span.Data[name] = logged
}

func LogTransaction(span *sentry.Span, name string, logged interface{}) {
	rootSpan := span.GetTransaction()
	if rootSpan == nil {
		return
	}

	Log(rootSpan, name, logged)
}

func Tag(span *sentry.Span, name string, value string) {
	span.SetTag(name, value)
}

func TagTransaction(span *sentry.Span, name string, value string) {
	rootSpan := span.GetTransaction()
	if rootSpan == nil {
		return
	}

	Tag(rootSpan, name, value)
}
