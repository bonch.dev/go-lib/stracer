package stracer

import (
	"github.com/getsentry/sentry-go"
	"github.com/gin-gonic/gin"
)

// ToGinContext injects span to *gin.Context.
func ToGinContext(ctx *gin.Context, span *sentry.Span) *gin.Context {
	ctx.Request = ctx.Request.WithContext(
		span.Context(),
	)

	return ctx
}

// WithGinContext returns current *gin.Context with injected in it span information and span itself.
func WithGinContext(ctx *gin.Context, t Type) (*gin.Context, *sentry.Span) {
	span := FromContext(ctx.Request.Context(), t)

	return ToGinContext(ctx, span), span
}
