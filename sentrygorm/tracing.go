package sentrygorm

import (
	"database/sql"
	"database/sql/driver"
	"errors"
	"fmt"
	"io"

	"github.com/getsentry/sentry-go"
	"gorm.io/gorm"

	"gitlab.com/bonch.dev/go-lib/stracer"
)

type sentryPlugin struct{}

func NewPlugin(opts ...Option) gorm.Plugin {
	p := &sentryPlugin{}
	for _, opt := range opts {
		opt(p)
	}

	return p
}

func (p sentryPlugin) Name() string {
	return "sentrygorm"
}

type gormHookFunc func(tx *gorm.DB)

type gormRegister interface {
	Register(name string, fn func(*gorm.DB)) error
}

func (p sentryPlugin) Initialize(db *gorm.DB) (err error) {
	cb := db.Callback()
	hooks := []struct {
		callback gormRegister
		hook     gormHookFunc
		name     string
	}{
		{cb.Create().Before("gorm:create"), p.before("gorm.Create"), "before:create"},
		{cb.Create().After("gorm:create"), p.after(), "after:create"},

		{cb.Query().Before("gorm:query"), p.before("gorm.Query"), "before:select"},
		{cb.Query().After("gorm:query"), p.after(), "after:select"},

		{cb.Delete().Before("gorm:delete"), p.before("gorm.Delete"), "before:delete"},
		{cb.Delete().After("gorm:delete"), p.after(), "after:delete"},

		{cb.Update().Before("gorm:update"), p.before("gorm.Update"), "before:update"},
		{cb.Update().After("gorm:update"), p.after(), "after:update"},

		{cb.Row().Before("gorm:row"), p.before("gorm.Row"), "before:row"},
		{cb.Row().After("gorm:row"), p.after(), "after:row"},

		{cb.Raw().Before("gorm:raw"), p.before("gorm.Raw"), "before:raw"},
		{cb.Raw().After("gorm:raw"), p.after(), "after:raw"},
	}

	var firstErr error

	for _, h := range hooks {
		if err := h.callback.Register("sentrygorm:"+h.name, h.hook); err != nil && firstErr == nil {
			firstErr = fmt.Errorf("callback register %s failed: %w", h.name, err)
		}
	}

	return firstErr
}

func (p *sentryPlugin) before(spanName string) gormHookFunc {
	return func(tx *gorm.DB) {
		tx.Statement.Context = InjectToContext(
			stracer.WithContext(
				tx.Statement.Context,
				stracer.WithCustomType("db.sql.query", "unknown"),
			),
		)
	}
}

func (p *sentryPlugin) after() gormHookFunc {
	return func(tx *gorm.DB) {
		span := ExtractFromContext(tx.Statement.Context)
		if span == nil {
			return
		}
		defer span.Finish()

		span.Description = tx.Dialector.Explain(
			tx.Statement.SQL.String(),
			tx.Statement.Vars...,
		)

		if tx.Statement.Table != "" {
			span.SetTag("table", tx.Statement.Table)
		}
		if tx.Statement.RowsAffected != -1 {
			span.SetTag("rows_affected", fmt.Sprintf("%d", tx.Statement.RowsAffected))
		}

		switch {
		case
			tx.Error == nil,
			errors.Is(tx.Error, gorm.ErrRecordNotFound),
			errors.Is(tx.Error, driver.ErrSkip),
			errors.Is(tx.Error, io.EOF),
			errors.Is(tx.Error, sql.ErrNoRows):
			// ignore
		default:
			span.SetTag("error", tx.Error.Error())
			span.Status = sentry.SpanStatusInternalError
		}
	}
}
