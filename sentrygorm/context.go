package sentrygorm

import (
	"context"

	"github.com/getsentry/sentry-go"
)

type spanContextKey struct{}

func InjectToContext(ctx context.Context, span *sentry.Span) context.Context {
	return context.WithValue(ctx, spanContextKey{}, span)
}

func ExtractFromContext(ctx context.Context) *sentry.Span {
	if span, ok := ctx.Value(spanContextKey{}).(*sentry.Span); ok {
		return span
	}
	return nil
}
