module gitlab.com/bonch.dev/go-lib/stracer/sentrylogrus

go 1.18

require (
	github.com/getsentry/sentry-go v0.28.1
	github.com/sirupsen/logrus v1.9.3
)

require (
	golang.org/x/sys v0.21.0 // indirect
	golang.org/x/text v0.16.0 // indirect
)
