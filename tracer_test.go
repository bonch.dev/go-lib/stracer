package stracer_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/bonch.dev/go-lib/stracer"
)

func TestFromContext(t *testing.T) {
	t.Run("FromContext - Returns span from empty context", func(t *testing.T) {
		ctx := context.Background()

		span := stracer.FromContext(ctx, stracer.WithType("Test"))

		assert.NotNil(t, span)
	})

	t.Run("FromContext - Returns span from injected context", func(t *testing.T) {
		ctx := context.Background()
		ctx, parentSpan := stracer.WithContext(ctx, stracer.WithType("Test - ParentSpan"))

		assert.NotNil(t, parentSpan)

		ctx, span := stracer.WithContext(ctx, stracer.WithType("Test"))

		assert.NotNil(t, span)
	})
}

func TestWithContext(t *testing.T) {
	t.Run("WithContext", func(t *testing.T) {
		ctx := context.Background()
		ctx, parentSpan := stracer.WithContext(ctx, stracer.WithType("Test - ParentSpan"))
		assert.NotNil(t, parentSpan)

		ctx, span := stracer.WithContext(ctx, stracer.WithType("Test"))

		assert.NotNil(t, ctx)
		assert.NotNil(t, span)
	})
}
