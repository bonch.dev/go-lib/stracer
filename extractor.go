package stracer

import (
	"context"

	"github.com/getsentry/sentry-go"
)

// SpanExtractor - type, used for extracting span from given context.
// It might be useful for extracting spans from contexts, which implements context.Value() function not clearly.
//
// (For example, *gin.Context can work only with keys, that defined as string)
type SpanExtractor func(ctx context.Context) *sentry.Span

var spanExtractors = []SpanExtractor{
	sentry.TransactionFromContext,
}

// ClearExtractors clears all extractors
func ClearExtractors() {
	spanExtractors = make([]SpanExtractor, 0)
}

// AddSpanExtractors adds additional extractors for extracting spans from context
func AddSpanExtractors(extractors ...SpanExtractor) {
	if spanExtractors == nil {
		spanExtractors = make([]SpanExtractor, 0)
	}

	spanExtractors = append(spanExtractors, extractors...)
}

func extractSpanFromContext(ctx context.Context) *sentry.Span {
	for _, extractor := range spanExtractors {
		if span := extractor(ctx); span != nil {
			return span
		}
	}

	return nil
}
